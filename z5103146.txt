Week 2 

Group formed and we created the Trello & Bitbucket accounts.
I wrote the background and aim section of the proposal.
Ask the tutor for advice and share the ideas with the team via email.
I discussed with the team about all available software tools and
libraries, and have a simple use of MetaMask, Remix - Solidity IDE and Ropsten Testnet.

Week 3

Discussed and Modified the epic part of the proposal with teammates.
I started to learn the tutorial and find some background material.
Set the plan with teammate.

Week 4 

Discussed about the tutorials and I did the first two tutorials.
After watching Dyric doing the pet shop tutorial, I did it myself.
Decided the plan before mid-break to create the user-login page and some 
functions about the seller's module.

Week 5

Built the user login webpage with my teammate, learnt to use the mongoDB to test 
the connection of sign-up function. Use the nodejs + express and Bootstrap to create the first page.
Go through the tutorial of building the shopping cart.

Week 6

Acheived the function that when the User's role is Seller, it will turn to Seller's page and 
the "Seller" user can post the items they want to sell to the home page which would be also stored
in the MongoDB.
Try to write the transaction part on Block Chain.

Week 7

Modified some information in MongoDB, and tested all functions to prepare for demo.
Debug the function of Buyers' module.

Week 8

Modified the seller update function and reduce by all function on the shopping cart page.
prepared some ppt for the final week 11 presentation.

Week 9

Improved the UI and tested the functions with team member Dyric, fixed some bugs like 404 error on the pages 
and made a plan about the final demo.

Week 10

Modified the front end pages with Ryan, finished the PPT for the presentation
Determined the role distribution in the next week's presentation. 
Improved the codes to make them more readable.

Week 11
I prepared for the presentation with Xin, and modified the whole codes to
make them more readable and presentable. Started to write the Descriptions of the functionalities
and Implementation challenges for the final report.
Tried the make to run the project using makefile.